# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['GnuHealthSequences']


class GnuHealthSequences(metaclass=PoolMeta):
    "Standard Sequences for GNU Health"
    __name__ = "gnuhealth.sequences"
    ambulatory_care_sequence = fields.Many2One('ir.sequence',
        'Health Ambulatory Care', domain=[
            ('code', '=', 'gnuhealth.ambulatory_care')
        ])
    patient_rounding_sequence = fields.Many2One('ir.sequence',
        'Health Rounding', domain=[
            ('code', '=', 'gnuhealth.patient_rounding')
        ])
